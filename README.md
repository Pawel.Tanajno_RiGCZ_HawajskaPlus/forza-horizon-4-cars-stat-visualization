# forza-horizon-4-cars-stat-visualization

Project for Data Exploration class

## Installation
You need Python 3.9 and last versions of packets:  
-beautifulsoup4  
-requests  
-pandas  
-openpyxl

## Usage
First run scrap_all_cars.py in order to get csv file with the data about all cars in Forza Horizon 4 game.

This script has to download over 600 webpages, so it might take 5-10 minutes for it to proceed, please be patient.

cars_data.csv is already available on repo if you had problems with scrapping script, but you can remove it since running python script will recreate it.

Next step is to run visualization.R line by line in order to see prepared plots. 
