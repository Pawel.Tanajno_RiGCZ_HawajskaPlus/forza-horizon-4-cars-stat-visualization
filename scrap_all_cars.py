from bs4 import BeautifulSoup
import requests
import pandas
import csv
import re


def read_best_cars(filename='Johnson_Racing_Tunes.xlsx'):
    """
    This functions parses data about the very best cars according to players from excel file
    :param filename: filename of excel file
    :return: dict of cars
    """
    data = pandas.read_excel(filename, sheet_name='Horizon 4', usecols='A:D')
    best_cars = {}
    for index, car in data.iterrows():
        if not pandas.isna(car['CAR']):
            car_name = car['CAR']
            car_name = re.sub(' \'[0-9]{2}', '', car_name)
            made_for = car['MADE FOR']
            if 'Cross' in made_for:
                made_for = 'Cross'
            elif 'Dirt' in made_for:
                made_for = 'Dirt'
            elif 'Snow' in made_for:
                made_for = 'Snow'
            elif 'Asphalt' in made_for:
                made_for = 'Asphalt'
            best_cars[car_name] = {'class': car['CLASS'], 'made for': made_for}
    return best_cars


def scrap_car_links():
    """
    Here we obrain url's for all cars from wiki main page
    :return: list of url's
    """
    url = "https://forza.fandom.com/wiki/Forza_Horizon_4/Cars"

    page = requests.get(url)

    bs = BeautifulSoup(page.content, 'html.parser')

    info = {}

    start = False
    stop = False

    for car in bs.find_all('tr'):
        start = True if not start and 'Abarth' in car.text else start
        stop = True if not stop and start and 'Zenvo TSR-S' in car.text else stop
        if start and not stop:
            for header in car.find_all('a', href=True):
                if header.get("class")  or 'LEGO' in header.text \
                        or 'Hot Wheels' in header.text or "Welcome Pack" in header.text:
                    pass
                elif header.get('href').count('/') > 2:
                    pass
                else:
                    car_href = 'https://forza.fandom.com' + header.get('href')
                    car_name = header.text
                    info[car_name] = car_href
    return info


def scrap_cars_information(car_links):
    """
    Function gets detailed car informations from Forza Horizon 4 wiki
    :param car_links: url's to car pages
    :return: dict {car name: {param_name: param_value ...} ...}
    """
    cars_list = {}
    for car, url in car_links.items():
        page = requests.get(url)
        bs = BeautifulSoup(page.content, 'html.parser')
        print(car)
        car_data = bs.find_all('div', class_="pi-data-value pi-font")
        try:
            cars_list[car] = {'year': car_data[1].text[:4], 'origin': car_data[2].text, 'engine': car_data[3].text,
                              'power': car_data[4].text[car_data[4].text.find('(') + 1:car_data[4].text.find('kW') - 1],
                              'torque': car_data[4].text[car_data[4].text.find('ft') + 5:car_data[4].text.find('N') - 1],
                              'engine layout': car_data[5].text.split(',')[0], 'drive': car_data[5].text.split(',')[1][1:],
                              'gears': car_data[6].text[0],
                              'weight': car_data[-1].text[car_data[-1].text.find('(') + 1:car_data[-1].text.find('kg') - 1]}
        except IndexError as e:
            cars_list[car] = {'year': '-', 'origin': '-', 'engine': '-', 'power': '-', 'torque': '-',
                              'engine layout': '-', 'drive': '-', 'gears': '-', 'weight': '-'}
    return cars_list


def save_in_csv(cars_list, best_cars):
    """
    This function saves all gathered data into csv file
    :param cars_list: list of all cars with detailed information
    :param best_cars: list of only the best cars and information about their specialization
    """
    with open('cars_data.csv', mode='w', newline='') as file:
        writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Car', 'Year', 'Origin', 'Engine', 'Power [kW]', 'Torque [N*m]', 'Engine layout', 'Drive',
                         'Gears', 'Weight [kg]', 'Best for', 'Class'])

        for car, info in cars_list.items():
            if car in best_cars.keys():
                best_for = best_cars[car]['made for']
                class_ = best_cars[car]['class']
            else:
                best_for = '-'
                class_ = '-'
            writer.writerow([car, info['year'], info['origin'], info['engine'], info['power'], info['torque'],
                             info['engine layout'], info['drive'], info['gears'], info['weight'], best_for, class_])


if __name__ == '__main__':
    best_cars = read_best_cars(filename='Johnson_Racing_Tunes.xlsx')
    car_links = scrap_car_links()
    cars_list = scrap_cars_information(car_links=car_links)
    save_in_csv(cars_list=cars_list, best_cars=best_cars)

